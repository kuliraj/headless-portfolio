### WordPress + React

Headless WordPress + React 

1.  A WordPress backend that serves its data via the WP REST API
2.  A server-side rendered React frontend using Next.js

### Prerequisites

*   **OS X:** You'll need [Homebrew](https://brew.sh/) and [Yarn](https://yarnpkg.com/en/) installed.
*   **Windows:** To install under Windows you need to be running the _64-bit version of Windows 10 Anniversary Update or later (build 1607+)_. The [Linux Subsystem for Windows](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide) should be installed and enabled before proceeding. Then, you'll need the prerequisites for Ubuntu Linux, detailed below, set up.
*   **Ubuntu Linux:** You'll need the latest version of NodeJS, Yarn and debconf-utils installed first. Follow this [simple guide](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions) to get the latest version of NodeJS installed. Install the rest of the packages using the `apt-get` package manager. _Note: During the WordPress installation, you may be asked to enter the root password at the prompt due to the use of the `sudo` command_
*   **Docker**: You'll need to install Docker [for your platform](https://www.docker.com/community-edition).

### Install

The following command will get WordPress running locally on your machine, along with the WordPress plugins you'll need to create and serve custom data via the WP REST API.

```zsh
> yarn install && yarn start
```

#### Install with Docker

```zsh
> yarn docker:build && yarn docker:start
```

When the installation process completes successfully:

*   The WordPress REST API is available at [http://localhost:8080](http://localhost:8080)
*   The WordPress GraphQL API is available at [http://localhost:8080/graphql](http://localhost:8080/graphql)
*   The WordPress admin is at [http://localhost:8080/wp-admin/](http://localhost:8080/wp-admin/) default login credentials `nedstark` / `winteriscoming`

### Extend the REST and GraphQL APIs

[custom REST API endpoints](https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/) in the Portfolio theme. 
[modify and extend the GraphQL API](https://wpgraphql.com/docs/getting-started/about).

The primary theme code is located in `wordpress/wp-content/themes/portfolio`.

## React Frontend

**Prerequisite:** Node 7 is required.

To spin up the frontend client app, run the following commands:

```zsh
> cd frontend && yarn install && yarn start
```

The Next.js app will be running on [http://localhost:3000](http://localhost:3000).

### Docker

To run the Docker container locally:

1.  Install [Docker](https://www.docker.com/) on your computer.
2.  In `frontend/config.js`, replace `localhost:8080` with your publicly-accessible WordPress installation's domain name or IP address.
3.  Start the container by running this command:

```zsh
> yarn run deploy
```

## Troubleshooting Common Errors

---
