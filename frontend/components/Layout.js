import { library, config } from '@fortawesome/fontawesome-svg-core';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faBehance, faTwitter, faDribbble, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import GlobalStyles from '../src/globalStyle';
import styled from 'styled-components';
import colorVariables from '../src/variables';
import Router from 'next/router'

Router.onRouteChangeStart = () => console.log('loading')
Router.onRouteChangeComplete = () => console.log('complete')
Router.onRouteChangeError = () => console.log('error')

config.autoAddCss = false;
library.add(faHeart, faBehance, faTwitter, faDribbble, faLinkedinIn);

const Layout = props => (
    <MainContainer  pageInner={props.pageInner}>
        <GlobalStyles/>
        {props.children}
    </MainContainer>
);

export default Layout;

const MainContainer = styled.div`
    position: relative;
    z-index: 1;
    overflow: hidden;
    -webkit-transform-origin: center 50vh;
    -moz-transform-origin: center 50vh;
    -ms-transform-origin: center 50vh;
    -o-transform-origin: center 50vh;
    transform-origin: center 50vh;
    -webkit-transition: -webkit-transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    -moz-transition: -moz-transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    transition: transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    pointer-events: none;
    min-height: 100vh;
    border: 10px solid white;
    width: calc(100vw - 20px);
`;