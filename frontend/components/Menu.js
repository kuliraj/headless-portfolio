import React, { Component } from "react";
import Link from "./ActiveLink.js";
import styled from 'styled-components';
import LogoSVG from '../src/icons/logo.js';
import DotsSVG from '../src/icons/menuDots.js';
import TimesSVG from '../src/icons/times.js';
import LogoActiveSVG from '../src/icons/logoActive.js';
import colorVariables from "../src/variables.js";

class Menu extends Component {
    constructor() {
        super();
    }
    getSlug(url) {
        const parts = url.split("/");
        return parts.length > 2 ? parts[parts.length - 2] : "";
    }

    render() {
        let Logo = <LogoSVG />;

        const menuItems = this.props.menu.items.map((item, index) => {
            if (item.object === "custom") {
                return (
                    <MenuSingleItem key={'menu-item-' + item.ID}>
                        <Link href={item.url} key={item.ID}>
                            <a>{item.title}</a>
                        </Link>
                    </MenuSingleItem>
                );
            }
            const slug = this.getSlug(item.url);
            const actualPage = item.object === "category" ? "category" : "post";
            return (
                <MenuSingleItem key={'menu-item-' + item.ID}>
                    <Link
                        as={item.object === 'post' || item.object === 'page' ? `/${slug}` : `/${item.object}/${slug}`}
                        href={`/${actualPage}?slug=${slug}&apiRoute=${item.object}`}
                        key={item.ID}
                    >
                        <a >{item.title}</a>
                    </Link>
                </MenuSingleItem>
            );
        });

        if (this.props.isVisible || this.props.inner) {
            Logo = <LogoActiveSVG />;
        }

        if (this.props.inner) {
            return(
                <Container>
                    <LogoWrapper inner={this.props.inner} isVisible={this.props.isVisible}>
                        <Link href="/">
                            <a>
                                {Logo}
                            </a>
                        </Link>
                    </LogoWrapper>
                    <MenuInnerWrapperDiv>
                        <MenuInner menuItems={menuItems}/>
                    </MenuInnerWrapperDiv>
                </Container>
            );
        } else {
            return(
                <Container>
                    <LogoWrapper inner={false} isVisible={this.props.isVisible}>
                        <Link href="/">
                            <a>
                                {Logo}
                            </a>
                        </Link>
                    </LogoWrapper>
                    <Anim key="animation" isVisible={this.props.isVisible} toggleMenu={this.props.toggleMenu} />
                    <MenuDots key="menu-dots" isVisible={this.props.isVisible} toggleMenu={this.props.toggleMenu}  />
                    <MenuPanel key="panel" menuItems={menuItems} isVisible={this.props.isVisible} />
                </Container>
            );
        }
    }
}

class MenuInner extends Component {
    render() {
        return (
            <MenuInnerWrapper>
                {this.props.menuItems}
            </MenuInnerWrapper>
        );
    }
}

const MenuInnerWrapperDiv = styled.div`
    text-align: right;
`;

const MenuInnerWrapper = styled.div`
    margin-top: 48px;
    display:inline-block;
    &:after {
        content: "";
        clear: both;
        display: table;
    }
    div {
        float: left;
        a {
            margin: 0 0 0 38px;
            font-size: 0.80em;
            letter-spacing: 5px;
            line-height: 15px;
            &:hover {
                font-weight: 700;
            }
        }
    }
`;

class MenuPanel extends Component {
    render() {
        return (
            <MenuItems isVisible={this.props.isVisible}>
                <MenuSingleItem key={'menu-item-frontpage'}>
                    <Link href="/">
                        <a >Home</a>
                    </Link>
                </MenuSingleItem>
                {this.props.menuItems}
            </MenuItems>
        );
    }
}

const MenuItems = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    z-index: 11;
    transition: opacity 1s ease;
    opacity: 0;
    height: 0;
    overflow: hidden;

    ${props => props.isVisible ? 'opacity: 1; height: auto;': ''};
`;

const MenuSingleItem = styled.div`
    text-align: center;
    a {
        text-transform: uppercase;
        text-decoration: none;
        color: ${colorVariables.themeDark};
        font-size: 1.125em;
        font-weight: 700;
        letter-spacing: 10px;
        line-height: 22px;
        display: inline-block;
        text-align: center;
        margin-bottom: 38px;
        &:hover {
            font-weight: 400;
            color: ${colorVariables.primaryColor};
        }
        &:focus, &:active {
            outline: none;
        }
        &.active {
            color: ${colorVariables.primaryColor};
            &:hover {
                font-weight: 700;
            }
        }
    }
`;

class Anim extends Component {
    render() {
        return (
            <AnimationWrapper isVisible={this.props.isVisible}>
                <NavBG isVisible={this.props.isVisible}></NavBG>
                <ToggleBtn
                    isVisible={this.props.isVisible}
                    onClick={this.props.toggleMenu} 
                    className={(this.props.isVisible ? 'shown' : '')}>
                </ToggleBtn>
            </AnimationWrapper>
        );
    }
}

const AnimationWrapper = styled.div`
    position: relative;
    visibility: ${props => props.isVisible ? 'visible' : 'hidden'};
    transition: all 0.25s ease 0s;
    margin: 0px;
    padding: 0px;
`;

const AnimBtn = styled.div`
    position: fixed;
    height: calc(60px);
    width: calc(60px);
    top: 58px;
    right: 58px;
    border-radius: 50%;
    background: rgb(255, 255, 255) none repeat scroll 0% 0%;
    cursor: pointer;
    margin: 0px;
    padding: 0px 15px;
    border: medium none;
    z-index: 11;
    -moz-user-select: none;
`;

const NavBG = styled(AnimBtn)`
    transform-origin: center center 0px;
    transition: transform 0.3s ease 0s;
    transform: ${props => props.isVisible ? 'translate(-700px,300px) scale(50)' : 'translate(0px, 0px) scale(1)'};
    will-change: transform;
    pointer-events: none;
`;

const ToggleBtn = styled(AnimBtn)`
    display: flex;
    flex-direction: column;
    -moz-box-pack: center;
    justify-content: center;
    -moz-box-align: center;
    align-items: center;
    color: rgb(78, 202, 120);
    transition: transform 0.3s ease 0s;
    &:before {
        content: "";
        transition: all 0.2s ease 0.2s;
        position: absolute;
        top: -3px;
        left: -1px;
        width: 100%;
        height: 100%;
        border-radius: inherit;
        filter: blur(5px);
        z-index: -2;
    }
    &:after {
        content: "";
        position: absolute;
        top: -3px;
        left: 0;
        width: 100%;
        height: 100%;
        border-radius: inherit;
        background: white none repeat scroll 0% 0%;
        z-index: -1;
    }
    &.shown {
        transform: scale(0.6);
        transition: all 0.2s ease 0s;
    }
`;

class MenuDots extends Component {
    render() {
        let menuText = 'Menu';
        let svg = <DotsSVG className="dots" />;
        
        if(this.props.isVisible) {
            menuText = 'Close';
            svg = <TimesSVG className="times" fill={colorVariables.themeDark} />;
        }
        
        const menuLabel = menuText.split('').map((letter, index) => {
            return (
                <MenuLabel key={'menu-label-' + index} index={index} isVisible={this.props.isVisible}>{letter}</MenuLabel>
            );
        });

        return (
            <MenuSvgContainer onClick={this.props.toggleMenu} >
                <MenuSvgWrapper >
                    {svg}
                </MenuSvgWrapper>
                <MenuDotsRadius isVisible={this.props.isVisible}></MenuDotsRadius>
                <div>
                    {menuLabel}
                </div>
            </MenuSvgContainer>
        );
    }
}

export default Menu;

const Container = styled.div`
    position: relative;
    width: 96%;
    max-width: 1400px;
    margin: 0 auto;
`;

const LogoWrapper = styled.div`
    position: fixed;
    top: 58px;
    left: 58px;
    z-index: 14;
    a {
        display:block;
        text-decoration: none;
        &:active,
        &:focus {
            outline: none;
        }
        &:hover {
            g {
                ${props => props.isVisible || props.inner ? 'fill:'+colorVariables.primaryColor : 'stroke:'+colorVariables.primaryColor};
            }
        }
    }
`;

const MenuSvgContainer = styled.div`
    position: fixed;
    top: 58px;
    right: 58px;
    display: block;
    width: 40px;
    height: 40px;
    z-index: 11;
    cursor: pointer;
    -webkit-transition: all 1.5s;
    -moz-transition: all 1.5s;
    -ms-transition: all 1.5s;
    -o-transition: all 1.5s;
    -webkit-transition: all 1.5s;
    transition: all 1.5s;
`;

const MenuSvgWrapper = styled.div`
    position: absolute;
    z-index: 11;
    height: 40px;

    &:hover .dots {
        .Top_left {
            transform: translate(8px, 8px);
        }
        .Top_right {
            transform: translate(-8px, 8px);
        }
        .Top_center {
            transform: translate(0px, 8px);
        }
        .Center_left {
            transform: translate(8px, 0px);
        }
        .Center_right {
            transform: translate(-8px, 0px);
        }
        .Bottom_left {
            transform: translate(8px, -8px);
        }
        .Bottom_right {
            transform: translate(-8px, -8px);
        }
        .Bottom_center {
            transform: translate(0px, -8px);
        }
    }

    svg {
        position: absolute;
        width: 40px;
        height: 40px;
        &.times {
            top: 14px;
            left: 14px;
            height: auto;
            width: auto;
        }
        &.dots {
            top: 2px;
            left: 0;
            .transitions {
                -webkit-transition: all .5s;
                -moz-transition: all .5s;
                -ms-transition: all .5s;
                -o-transition: all .5s;
                transition: all .5s;
            }
        }
    }
`;

const MenuDotsRadius = styled.div`
    position: absolute;
    right: 0px;
    border-radius: 50%;
    width: 40px;
    height: 40px;
    opacity: 0.2;
    border: 1px solid ${props => props.isVisible ? colorVariables.themeDark : 'white'};
    z-index: 10;
    
    ${MenuSvgContainer}:hover & {
        opacity: 1;
    }
`;

const MenuLabel = styled.div`
    position: absolute;
    width: 40px;
    height: 40px;
    visibility: hidden;
    color: ${props => props.isVisible ? colorVariables.themeDark : 'white'};
    top: 13px;
    right: 40px;
    font-size: 0.75em;
    letter-spacing: 2px;
    z-index: 8;
    opacity: 0;
    transition: all .5s;
    font-weight: 700;
    text-transform: uppercase;

    ${MenuSvgContainer}:hover & {
        width: 50px;
        height: 20px;
        visibility: inherit;
        opacity: 1;
    }

    &:nth-child(1) {
        transition: all 0.5s;
        right: ${props => props.isVisible ? '61px' : '58px'};
    }
    &:nth-child(2) {
        transition: all .7s;
        right: ${props => props.isVisible ? '49px' : '43px'};
    }
    &:nth-child(3) {
        transition: all .9s;
        right: ${props => props.isVisible ? '40px' : '33px'};
    }
    &:nth-child(4) {
        transition: all 1.1s;
        right: ${props => props.isVisible ? '27px' : '20px'};
    }
    &:nth-child(5) {
        transition: all 1.3s;
        right: 17px;
    }
`;