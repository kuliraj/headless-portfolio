import React, { Component } from "react"
import Link from "next/link"
import styled, { keyframes } from "styled-components"
import colorVariables from '../src/variables'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Footer extends Component {
    constructor() {
        super();
    }

    render() {
        let socialItems = [];
        let madeWithLove = ''; 

        for (let key in this.props.options.social_media) {
            let value = this.props.options.social_media[key];
            if (!this.props.isVisible && this.props.inner) {
                socialItems.push(
                    <FooterListItemInner key={'social-' + key} isVisible={this.props.isVisible}>
                        <Link href={value} >
                            <a target="_blank"><FontAwesomeIcon icon={['fab', (key == 'linkedin' ? 'linkedin-in' : key)]} /></a>  
                        </Link>
                    </FooterListItemInner>
                );
            } else {
                socialItems.push(
                    <FooterListItem key={'social-' + key} isVisible={this.props.isVisible}>
                        <Link href={value} >
                            <a target="_blank"><FontAwesomeIcon icon={['fab', (key == 'linkedin' ? 'linkedin-in' : key)]} /><span>{key}</span>	 </a>  
                        </Link>
                    </FooterListItem>
                );
            }
        }

        if (socialItems.length && (!this.props.inner || this.props.isVisible)) {
            socialItems.push(
                <FooterListAfter key='social--after' inner={this.props.inner} isVisible={this.props.isVisible}></FooterListAfter>
            );
        }

        if (!this.props.inner || this.props.isVisible) {
            madeWithLove = <FooterParagraph >Made with <Heart><FontAwesomeIcon icon={['fas', 'heart']} /></Heart></FooterParagraph>;
        }

        return (
            <FooterWrapper inner={this.props.inner}>
                <FooterList>
                    {socialItems}
                </FooterList>
                <CopyRights>
                    {madeWithLove}
                    <FooterParagraph isVisible={this.props.isVisible} inner={this.props.inner} >Tsvetelina Lazarova - All Rights Reserved  ©  {(new Date().getFullYear())}</FooterParagraph>
                </CopyRights>
            </FooterWrapper>
        );
    }
}

export default Footer;

const FooterWrapper = styled.div`
`;

const FooterList = styled.ul`
    position: fixed;
    bottom: 58px;
    left: 68px;
    list-style: none;
    padding: 0;
    margin: 0;
    z-index: 14;
`;
const FooterListItemInner = styled.li`
    padding-right: 25px;
    display: inline-block;
    a {
        display: inline-block;
        color: ${colorVariables.themeDark};
        text-decoration: none;
        &:hover {
            color: ${colorVariables.primaryColor};
        }
        &:active, &:focus {
            outline: 0;
        }
    }
`;
const FooterListItem = styled.li`
    padding-bottom: 20px;
    a {
        color: ${props => props.isVisible ? colorVariables.themeDark : colorVariables.white};
        text-decoration: none;
        &:hover {
            color: ${colorVariables.primaryColor};
            span {
                opacity: 1;
                transition: opacity 0s ease-in 500ms, opacity 500ms;
                &:after{
                    width: 0;   
                }
            }
        }
        &:active, &:focus {
            outline: 0;
        }
        span {
            color: ${props => props.isVisible ? colorVariables.themeDark : colorVariables.white};
            font-size: 0.75em;
            padding-left: 10px;
            opacity: 0;
            transition: opacity 0s ease-in 500ms, opacity 500ms;
            position: relative;
            &:after{
                display: block;
                content: '';
                position: absolute;
                top: 0;
                right: 0;
                width: 100%;
                height: 100%;
                background-color: ${props => props.isVisible ? colorVariables.white : colorVariables.themeDark};
                transition: width 500ms;
            }
        }
    }
`;
const FooterListAfter = styled.li`
    margin-left: 6px;
    background-color: ${props => props.isVisible ? colorVariables.themeDark : colorVariables.white};
    width: 1px;
    height: 47px;
`;
const CopyRights = styled.div`
    position: fixed;
    bottom: 58px;
    right: 58px;
    color: ${colorVariables.themeDark};
    font-size: 0.75em;
    z-index: 14;
`;

const FooterParagraph = styled.p`
    line-height: 20px;
    text-align: right;
    margin: ${props => (!props.inner || props.isVisible) ? '10px 0 0 0' : '0'};
    &:first-child {
        position: relative;
        padding-right: 24px;
    }
`;

const smallBeat = 'font-size: 18px';
const bigBeat = 'font-size: 20px';
const beat = keyframes`
    0% {
        ${smallBeat}
    }
    40% {
       ${bigBeat}
    }
    60% {
        ${smallBeat}
    }
    80% {
       ${bigBeat}
    }
    100% {
        ${smallBeat}
    }
`;

const Heart = styled.span`
    position: absolute;
    color: ${colorVariables.primaryColor};
    ${bigBeat};
    padding-left: 5px;
    animation: ${beat} 1s infinite;
`;
