import React from "react";
import { Config } from "../config.js";
import cachedFetch, { overrideCache } from '../utils/cachedFetch';

const PageWrapper = Comp => (
  class extends React.Component {
    static async getInitialProps(args) {
      const headerMenu = await cachedFetch(
        `${Config.apiUrl}/wp-json/menus/v1/menus/header-menu`
      );
      const headlessOptions = await cachedFetch(
        `${Config.apiUrl}/wp-json/acf/v3/options/headless-settings`
      );
      return {
        headerMenu,
        headlessOptions,
        ...(Comp.getInitialProps ? await Comp.getInitialProps(args) : null),
      };
    }

    render() {
      return (
        <Comp {...this.props} />
      )
    }
  }
)

export default PageWrapper;
