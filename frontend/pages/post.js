import Layout from "../components/Layout.js";
import React, { Component } from "react";
import fetch from "isomorphic-unfetch";
import Error from "next/error";
import PageWrapper from "../components/PageWrapper.js";
import Menu from "../components/Menu.js";
import { Config } from "../config.js";
import styled from "styled-components";
import colorVariables from '../src/variables';
import Footer from "../components/Footer.js";

class Post extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            pageInner: true
        }
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    static async getInitialProps(context) {
        const { slug, apiRoute } = context.query;
        const res = await fetch(
            `${Config.apiUrl}/wp-json/portfolio/v1/${apiRoute}?slug=${slug}`
        );
        const post = await res.json();
        return { post };
    }
     
    componentDidUpdate(prevProps, prevState) {
        if(prevProps !== this.props){
            this.setState({isVisible: false});
        }
    }

    toggleMenu(e) {
        e.preventDefault();

        if (typeof this.state.isVisible === 'undefined'|| false === this.state.isVisible ) {
            this.setState({isVisible: true});
        } else {
            this.setState({isVisible: false});
        }
    }

    render() {
        if (!this.props.post.title) return <Error statusCode={404} />;

        return (
            <Layout pageInner={this.state.pageInner}>
                <MainWrapper>
                    <Menu
                        pageInner={this.state.pageInner}
                        menu={this.props.headerMenu}
                        toggleMenu={this.toggleMenu}
                        isVisible={this.state.isVisible}
                    />
                    <Container>
                        <h1>{this.props.post.title.rendered}</h1>
                        <div className='content' dangerouslySetInnerHTML={{
                            __html: this.props.post.content.rendered
                        }}></div>
                    </Container>
                    <Footer
                        pageInner={this.state.pageInner}
                        options={this.props.headlessOptions.acf}
                        isVisible={this.state.isVisible}
                    />
                </MainWrapper>
            </Layout>
        );
    }
}

export default PageWrapper(Post);

const MainWrapper = styled.div`
    min-height: calc(100vh - 20px);
    display: grid;
    grid-template-rows: auto 1fr auto;
    color: ${colorVariables.themeDark};
`;

const Container = styled.div`
    position: relative;
    width:96%;
    max-width: 1400px;
    margin: 0 auto;
`;