import Layout from "../components/Layout.js";
import React, { Component } from "react";
import fetch from "isomorphic-unfetch";
import Link from "next/link";
import PageWrapper from "../components/PageWrapper.js";
import Menu from "../components/Menu.js";
import Footer from "../components/Footer.js";
import { Config } from "../config.js";
import styled from "styled-components";
import colorVariables from '../src/variables';
import HelloSVG from '../src/icons/hello.js';

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: false,
            teetotumed: false,
            pageInner: false
        }
        this.toggleMenu = this.toggleMenu.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.lastScrollTop = 0;
    }

    static async getInitialProps(context) {
        const pageRes = await fetch(
            `${Config.apiUrl}/wp-json/portfolio/v1/frontpage`
        );
        const page = await pageRes.json();

        return { page };
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(e) {
        var st = window.pageYOffset || document.documentElement.scrollTop; 
        console.log(st, this.lastScrollTop);
        
        if (st > this.lastScrollTop && !this.state.teetotumed){
            this.setState({
                teetotumed: true
            });

        } else if ( st == 0 && this.state.teetotumed ) {
            this.setState({
                teetotumed: false
            });
        }
        this.lastScrollTop = st;
    }

    toggleMenu(e) {
        e.preventDefault();

        if (typeof this.state.isVisible === 'undefined'|| false === this.state.isVisible ) {
            this.setState({isVisible: true});
        } else {
            this.setState({isVisible: false});
        }
    }

    render() {

        return (
            <Layout teetotumed={this.state.teetotumed}>
                <MainWrapper>
                    <Menu
                        menu={this.props.headerMenu}
                        toggleMenu={this.toggleMenu}
                        isVisible={this.state.isVisible}
                        teetotume={this.state.teetotumed}
                    />
                    <PageHome>
                        <Intro>
                            <Hello>
                                <HelloInnerWrapper teetotumed={this.state.teetotumed}>
                                    <Heading>
                                        <HelloSVG />
                                    </Heading>
                                    <IntroColor teetotumed={this.state.teetotumed}></IntroColor>
                                </HelloInnerWrapper>
                            </Hello>
                            <Teetotum>
                                <InnerWrapper>
                                    <Title>{this.props.page.title.rendered}</Title>
                                    <SubTitle
                                        dangerouslySetInnerHTML={{
                                            __html: this.props.page.content.rendered
                                        }}
                                    />
                                    <Centered>
                                        <div>
                                            <Link href={'/about'} >
                                                <Button className="ala" title="Hire Me">Hire Me</Button>  
                                            </Link>
                                            <Link href={'/about'} >
                                                <ButtonRight className="bala" title="My Work">My Work</ButtonRight>  
                                            </Link>
                                        </div>
                                    </Centered>
                                    
                                </InnerWrapper>
                            </Teetotum>
                        </Intro>
                    </PageHome> 
                    <AboutWrapper>
                        <Title>{this.props.page.title.rendered}</Title>
                        <SubTitle
                            dangerouslySetInnerHTML={{
                                __html: this.props.page.content.rendered
                            }}
                        />
                        <Centered>
                            <div>
                                <Link href={'/about'} >
                                    <Button className="ala" title="Hire Me">Hire Me</Button>  
                                </Link>
                                <Link href={'/about'} >
                                    <ButtonRight className="bala" title="My Work">My Work</ButtonRight>  
                                </Link>
                            </div>
                        </Centered>
                    </AboutWrapper>
                    <Footer
                        options={this.props.headlessOptions.acf}
                        isVisible={this.state.isVisible}
                    />
                </MainWrapper>
            </Layout>
        );
    }
}

export default PageWrapper(Index);

const MainWrapper = styled.div`
    position: relative;
    background: ${colorVariables.white};
    overflow: hidden;
    -webkit-transition: -webkit-transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    -moz-transition: -moz-transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    transition: transform 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    pointer-events: all;
    min-height: calc(100vh - 20px);
`;

const PageHome = styled.div`
    position: relative;
    min-height: 100vh;
    z-index: 10;
    overflow: hidden;
    -webkit-transition: -webkit-transform 1000ms cubic-bezier(1, 0, 0, 1) 0ms, padding 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    -moz-transition: -moz-transform 1000ms cubic-bezier(1, 0, 0, 1) 0ms, padding 500ms cubic-bezier(1, 0, 0, 1) 0ms;
    transition: transform 1000ms cubic-bezier(1, 0, 0, 1) 0ms, padding 500ms cubic-bezier(1, 0, 0, 1) 0ms;
`;

const Intro = styled.div`
    position: relative;
    background: transparent;
    pointer-events: none;
    overflow: visible;
    z-index: 0;
`;

const Hello = styled.div`
    position: fixed;
    left: 10px;
    right: 10px;
    height: calc(100vh - 20px);;
    z-index: 2;
    pointer-events: none;
    overflow: hidden;
    transform: matrix(1, 0, 0, 1, 0, 0);
`;

const Teetotum = styled.div`
    position: fixed;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -moz-box-align: center;
    box-align: center;
    -webkit-align-items: center;
    -moz-align-items: center;
    -ms-align-items: center;
    -o-align-items: center;
    align-items: center;
    -ms-flex-align: center;
    background: ${colorVariables.primaryColorLight};
    width: calc(100vw - 20px);
    height: 100vh;
    z-index: 1;
    transform: matrix(1, 0, 0, 1, 0, 0);
`;


const InnerWrapper = styled.div`
    position: relative;
    width: calc(100vw - 20px);
    z-index: 10;
    padding: 20vh 165px 20vh 22%;
`;
const HelloInnerWrapper = styled(InnerWrapper)`
    height: 100vh;
    transform: ${props => !props.teetotumed ? 'translateX(0%)' : 'translateX(50%)'};
    transition: transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 0ms;
`;

const Heading = styled.h1`
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    white-space: nowrap;
    margin: 0px;
    z-index: 1;
    
    svg {
        width: 478px;
        height: 404px;
    }

`;

const IntroColor = styled.div`
    background: ${colorVariables.white};
    position: absolute;
    top: 0;
    left: 50%;
    right: 0;
    bottom: 0;
    z-index: 0;
    pointer-events: none;
    height: 100vh;
    transform: ${props => !props.teetotumed ? 'translateX(0%)' : 'translateX(50%)'};
    -webkit-transition: -webkit-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 50ms;
    -moz-transition: -moz-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 50ms;
    transition: transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 50ms;
`;

const Title = styled.h1`
    margin-top: 0;
    letter-spacing: 18px;
    line-height: 34px;
    text-align: center;
    text-transform: uppercase;

    -webkit-transition: -webkit-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 0ms, opacity 500ms linear 450ms;
    -moz-transition: -moz-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 0ms, opacity 500ms linear 450ms;
    transition: transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 0ms, opacity 500ms linear 450ms;


`;

const SubTitle = styled.div`
    letter-spacing: 4px;
    line-height: 26px;
    text-align: center;
    color: ${colorVariables.white};
    margin: 42px 0;

    -webkit-transition: -webkit-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 150ms, opacity 500ms linear 600ms;
    -moz-transition: -moz-transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 150ms, opacity 500ms linear 600ms;
    transition: transform 1000ms cubic-bezier(0.7, 0, 0.3, 1) 150ms, opacity 500ms linear 600ms;
`;

const Centered = styled.div`
    display: grid;
    justify-items: center;
`;

const Button = styled.a`
    display: inline-block;
    background-color: ${colorVariables.primaryColor};
    border: 2px solid ${colorVariables.primaryColor};
    color: ${colorVariables.white};
    font-size: 0.75em;
    font-weight: 700;
    text-transform: uppercase;
    padding: 10px 38px;
    border-radius: 25px;
    margin: 0 6px;
    letter-spacing: 1.2px;
    cursor: pointer;
    
    &:hover {
        background-color: ${colorVariables.themeDark};
    }
`;

const ButtonRight = styled(Button)`
    background-color: ${colorVariables.themeDark};
    &:hover {
        background-color: ${colorVariables.primaryColor};
    }
`;

const AboutWrapper = styled.div`
    position: relative;
    margin-top: 200vh;
    z-index: 10;
    transform: translate3D(0, 0, 0);
`;