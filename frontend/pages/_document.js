import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet } from 'styled-components';

export default class MyDocument extends Document {
  static async getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  render() {
    return (
      <html>
      <Head>
            <meta
                name="viewport"
                content="width=device-width, initial-scale=1"
            />
            <meta charSet="utf-8" />
            <title>
                Tsvetelina Lazarova | Portfolio
            </title>
            <link rel="icon" type="image/x-icon" href="./static/favicon.ico" />
            {this.props.styleTags}
            <style dangerouslySetInnerHTML={{__html: `
              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-Hairline.ttf') format('truetype');
                font-weight: 100;
                font-style: normal;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-HairlineItalic.ttf') format('truetype');
                font-weight: 100;
                font-style: italic;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-Light.ttf') format('truetype');
                font-weight: 300;
                font-style: normal;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-LightItalic.ttf') format('truetype');
                font-weight: 300;
                font-style: italic;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-Regular.ttf') format('truetype');
                font-weight: 400;
                font-style: normal;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-RegularItalic.ttf') format('truetype');
                font-weight: 400;
                font-style: italic;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-Bold.ttf') format('truetype');
                font-weight: 700;
                font-style: normal;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-BoldItalic.ttf') format('truetype');
                font-weight: 700;
                font-style: italic;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-Black.ttf') format('truetype');
                font-weight: 900;
                font-style: normal;
              }

              @font-face {
                font-family: 'Lato';
                src: url('./static/fonts/Lato/Lato-BlackItalic.ttf') format('truetype');
                font-weight: 900;
                font-style: italic;
              }
            `}}/>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
