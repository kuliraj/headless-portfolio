const colorVariables = {
    'white': '#fff',
    'themeGreyLigh': '#9B9B9B',
    'themeGrey': '#717171',
    'themeGreyDarker': '#4A4A4A',
    'themeDark': '#211F1F',
    'primaryColorLight': '#BD617E',
    'primaryColor': '#BE466C',
    'primaryColorDarker': '#911D42'
};

export default colorVariables;