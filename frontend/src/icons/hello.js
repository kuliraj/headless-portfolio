import React from "react";

const SVG = ({
  style = {},
  width = "478px",
  height = "404px",
  viewBox = "0 0 478 404"
}) => (
  <svg
    width={width}
    height={height}
    viewBox={viewBox}
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" fontFamily="Lato-Black, Lato" fontSize="220" fontWeight="700" letterSpacing="10" lineSpacing="240">
        <g transform="translate(-727.000000, -348.000000)">
            <text id="HE-LLO.">
                <tspan x="714" y="486" fill="#211F1F">HE</tspan>
                <tspan x="714" y="726" fill="#211F1F">LLO</tspan>
                <tspan x="1131.26" y="661.361328" fontSize="320" fill="#BD617E">.</tspan>
            </text>
        </g>
    </g>
  </svg>
);

export default SVG;
