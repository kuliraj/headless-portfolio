import React from "react";

const SVG = ({
  style = {},
  fill = "#231E1F",
  className = "",
  width = '13px',
  height = '13px'
}) => (
  <svg
    width={width}
    height={height}
    style={style}
    viewBox="0 0 13 13"
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${className || ""}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-1277.000000, -64.000000)" fill={fill}>
            <g transform="translate(-1331.000000, -843.000000)">
                <g >
                    <g transform="translate(2594.000000, 893.000000)">
                        <polygon points="26.3398438 15.8515625 21.6523438 20.5 26.3398438 25.1484375 25.1679688 26.3203125 20.4804688 21.671875 15.8320312 26.3203125 14.6601562 25.1484375 19.3085938 20.5 14.6601562 15.8515625 15.8320312 14.6796875 20.4804688 19.328125 25.1679688 14.6796875"></polygon>
                    </g>
                </g>
            </g>
        </g>
    </g>
  </svg>
);

export default SVG;
