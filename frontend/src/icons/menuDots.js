import React from "react";

const SVG = ({
  style = {},
  fill = "#fff",
  className = "",
}) => (
  <svg
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    className={`svg-icon ${className || ""}`}
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g fill={fill}>
      <circle cx="12" cy="10.1" r="2" className="transitions Top_left"></circle>
      <circle cx="27.9" cy="10.1" r="2" className="transitions Top_right"></circle>
      <circle cx="20" cy="10.1" r="2" className="transitions Top_center"></circle>
      <circle cx="12" cy="18" r="2" className="transitions Center_left"></circle>
      <circle cx="27.9" cy="18" r="2" className="transitions Center_right"></circle>
      <circle cx="20" cy="18" r="2"></circle>
      <circle cx="12" cy="26" r="2" className="transitions Bottom_left"></circle>
      <circle cx="27.9" cy="26" r="2" className="transitions Bottom_right"></circle>
      <circle cx="20" cy="26" r="2" className="transitions Bottom_center"></circle>
    </g>
  </svg>
);

export default SVG;
